package hu.szakdolgozat.tanya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@SuppressWarnings("deprecation")
@SpringBootApplication
public class TanyaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TanyaApplication.class, args);
		System.out.println("Start");
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200/")
                        .allowedHeaders("Content-Type", "X-Requested-With", "Authorization")
                        .allowedMethods("GET", "POST", "DELETE", "PUT", "OPTIONS").allowCredentials(true);
            };
        };
	}
}
